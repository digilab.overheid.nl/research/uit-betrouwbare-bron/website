---
title: Papier, pixels en procesketens - de onzichtbare revolutie bij de overheid
draft: false
date: 2025-02-2025
description: Papier, pixels en procesketens - werkende ketens voor iedereen (blogpost in het kader van 'Dag van de toekomstige ketens')
weight: 1
layout: single
preliminary: false
category: project
doc-type: blog
---

**De digitalisering van de overheid lijkt op het eerste gezicht een simpele moderniseringsslag: van
papier naar pixels, van loketten naar websites. Maar onder deze ogenschijnlijk eenvoudige transitie
gaat een fundamentele transformatie schuil die de manier waarop de overheid functioneert drastisch
heeft veranderd. In deze blog verkennen we hoe de overstap van fysieke naar digitale processen niet
alleen onze interactie met de overheid heeft veranderd, maar ook heeft geleid tot onvoorziene
uitdagingen in de uitvoering van overheidstaken.**

## Van papier naar pixels

De uitvoering van overheidstaken is diep geworteld in een papieren traditie. Dit is meer dan alleen
een kwestie van documentatie - het heeft de hele architectuur van overheidsprocessen gevormd. Denk
aan de fysieke dossiers in archiefkasten, formulieren die met de post werden verzonden, en
handtekeningen die met pen op papier werden gezet. Deze papieren werkelijkheid creëerde natuurlijke
grenzen: documenten konden maar op één plek tegelijk zijn, moesten fysiek worden overgedragen, en
het aantal mensen dat tegelijk toegang had tot informatie was beperkt door praktische beperkingen.

| <img src="../../img/2462-1832.png" alt="Voorbeeld Kadaster in 1832"> |
|:--------------------------------------------------------------------:|
| Figuur 1: Kadasterkantoor als voorbeeld als fysiek instituut         |

Met de komst van digitale technologie is deze realiteit fundamenteel veranderd. Wat ooit bestond als
stapels papier in archiefkasten, bestaat nu als datastromen in serverparken. Deze transformatie
biedt ongekende mogelijkheden: documenten kunnen instant worden gedeeld, duizenden aanvragen kunnen
parallel worden verwerkt, en informatie is overal en altijd toegankelijk. Maar deze digitalisering
is vaak als een deken over bestaande processen heen gelegd, zonder de onderliggende werkwijze
grondig te herzien.

De schaalvergroting die hieruit voortvloeit is monumentaal. Waar een ambtenaar vroeger tientallen
dossiers per dag kon verwerken, handelen geautomatiseerde systemen nu duizenden gevallen per uur af.
Deze exponentiële schaalvergroting heeft echter een keerzijde: als er fouten in het systeem zitten,
vermenigvuldigen deze zich ook op dezelfde schaal. Een verkeerd geprogrammeerde regel kan in korte
tijd duizenden burgers raken.

| <img src="../../img/2462-2010.png" alt="Voorbeeld Kadaster in 2010">        |
|:---------------------------------------------------------------------------:|
| Figuur 2: Kadaster als voorbeeld als half fysiek en half digitaal instituut |

Deze digitale transformatie heeft ook het karakter van overheidsorganisaties fundamenteel veranderd.
De imposante overheidsgebouwen die ooit symbool stonden voor bureaucratische macht en afstand,
vervagen nu in een netwerk van servers en clouds. Paradoxaal genoeg wordt de overheid hierdoor
tegelijkertijd ongrijpbaarder én toegankelijker. Via websites en apps staat ze 24/7 tot onze
beschikking, maar de menselijke factor - die ambtenaar achter het loket die nog kon meedenken en
maatwerk kon leveren - verdwijnt steeds meer naar de achtergrond.

| <img src="../../img/2462-2032.png" alt="Voorbeeld Kadaster in 2032">   |
|:----------------------------------------------------------------------:|
| Figuur 3: Kadaster als voorbeeld als digitaal instituut in het netwerk |

## Procesketens

Deze fundamentele verschuiving van papier naar digitaal heeft misschien wel de grootste impact op
ketens van processen binnen en tussen organisaties. In het papieren tijdperk was de fysieke route
van een dossier glashelder: je kon letterlijk volgen hoe een map van bureau naar bureau ging, van
afdeling naar afdeling, vaak vergezeld van een routeformulier met handtekeningen en datumstempels.
Deze fysieke beperkingen zorgden voor een natuurlijk ritme en creëerden vanzelf momenten van
controle en reflectie.

In de digitale wereld zijn deze ketens ogenschijnlijk efficiënter geworden. Een document kan
instantaan worden gedeeld met alle betrokken partijen, wijzigingen worden realtime gesynchroniseerd,
en processtromen kunnen parallel lopen in plaats van sequentieel. Maar deze ogenschijnlijke
efficiëntie verhult fundamentele problemen. Waar eerder een fysieke map niet 'kwijt' kon raken - hij
was altijd ergens - kunnen digitale dossiers verdwijnen in de krochten van verschillende systemen
die niet met elkaar praten. De verantwoordelijkheid voor een proces vervaagt als tien afdelingen
tegelijk toegang hebben tot dezelfde informatie.

Nog complexer wordt het wanneer verschillende organisaties in deze ketens moeten samenwerken. Waar
vroeger een papieren dossier van de ene naar de andere organisatie ging, met duidelijke
overdrachtsprotocollen, zien we nu een web van digitale verbindingen ontstaan. Gemeentes,
uitvoeringsorganisaties, ministeries - allemaal hebben ze hun eigen systemen, die vaak met kunst- en
vliegwerk aan elkaar geknoopt worden. Het resultaat is een digitale lappendeken waar informatie
regelmatig tussen de naden door glipt.

| <img src="../../img/2462-2032-kadaster-in-het-netwerk.png" alt="Voorbeeld Kadaster in de digitale lappendeken"> |
|:---------------------------------------------------------------------------------------------------------------:|
| Figuur 4: Kadaster als voorbeeld in de digitale lappendeken                                                     |

De toekomst van deze procesketens roept fundamentele vragen op. Moeten we vasthouden aan de oude,
lineaire procesmodellen die we hebben geërfd uit het papieren tijdperk? Of vraagt de digitale
realiteit om een complete herziening van hoe we processen organiseren? Kunnen we bijvoorbeeld toe
naar een model waarin processen meer organisch verlopen, gestuurd door de behoefte van het moment in
plaats van vooraf vastgelegde routes?

En misschien wel de meest cruciale vraag: hoe waarborgen we in deze nieuwe realiteit belangrijke
waarden als transparantie, controleerbaarheid en menselijk maatwerk? Want hoewel digitalisering
enorme mogelijkheden biedt voor efficiëntie en toegankelijkheid, mogen we niet vergeten dat achter
elk proces uiteindelijk mensen zitten - zowel als uitvoerder als als burger.

De uitdaging voor de komende jaren zal zijn om een nieuwe balans te vinden. Een balans tussen de
mogelijkheden van digitale technologie en de menselijke maat. Tussen efficiëntie en zorgvuldigheid.
Tussen gestandaardiseerde processen en ruimte voor maatwerk. Want uiteindelijk draait
overheidshandelen niet om papier of pixels, maar om het effectief dienen van de samenleving.

Goed, dat gaan we niet in één dag oplossen. Maar laten we beginnen met het
gesprek over onze procesketens, welke issues daarin nu prominent spelen en hoe
we stappen kunnen maken in het oplossen daarin. Tijdens de _**Dag van de
toekomstige ketens**_ willen we samen met jullie, aan de hand van casuïstiek en
met speciale aandacht voor de rol van capabele registers, verkennen wat er nodig
is om stappen te kunnen maken. Binnenkort volgt er meer informatie over de inhoud van
deze dag. Zet 'm vast in je agenda: **3 april 2025**!

_**Auteur**: Marc van Andel is solution architect en innovator bij het
[Kadaster](https://www.kadaster.nl/zakelijk/over-ons/innovatie). Op dit moment
is hij gedetacheerd bij het [programma
RealisatieIBDS](https://realisatieibds.nl), specifiek het ontstaan van het
[Federatief
Datastelsel](https://realisatieibds.nl/page/view/564cc96c-115e-4e81-b5e6-01c99b1814ec/de-ontwikkeling-van-het-federatief-datastelsel).
Als onderdeel daarvan is Marc ook betrokken bij het [project Uit betrouwbare
bron](https://uitbetrouwbarebron.nl)._
