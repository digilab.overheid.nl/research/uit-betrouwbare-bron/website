---
title : "Uit betrouwbare bron — Onderzoekt en beproeft capabele overheidsregisters"
description: "‘Uit betrouwbare bron’ onderzoekt en beproeft hoe we overheidsgegevens moeten registreren om gebruik ‘bij de bron’ (eenvoudig) mogelijk te maken." 
lead: "Uit betrouwbare bron"
date: 2023-05-23T08:47:36+00:00
---
