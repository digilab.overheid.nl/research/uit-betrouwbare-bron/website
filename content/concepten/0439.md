---
title: Vernietigen op basis van bewaartermijnen
doc-type: capability
category: concepten
navigation: Capability's
draft: false
description: Definitie van de capability 'een capabel register kan vernietigen op basis van bewaartermijnen'
layout: single
preliminary: false
weight: 26
---

_'Een capabel register kan vernietigen op basis van bewaartermijnen'_ betekent dat het register gegevens kan vernietigen nadat de daarvoor geldende Archiefwettelijke bewaartermijn is verstreken.

<img src="../../img/0439-capability-vernietigen-op-basis-van-bewaartermijnen.svg" alt="Relaties van capability 'een capabel register kan vernietigen op basis van bewaartermijnen" title="Relaties van capability 'een capabel register kan vernietigen op basis van bewaartermijnen" style="width: 15%;">

De capability _'een capabel register kan vernietigen op basis van bewaartermijnen'_ is gerelateerd aan:

- de waarde [integriteit](../1592) omdat de verplichting gegevens na verlopen van de Archiefwettelijke bewaartermijn 'onherstelbaar' te vernietigen, mogelijk conflicteert met de gewenste integriteit van het register, en
- het requirement ['het prototyperegister volgt het 'append only'-patroon'](../../prototype/9693) omdat, afhankelijk van de invulling die aan het woord 'onherstelbaar' wordt gegeven, 'onherstelbaar' vernietigen in een 'append-only'-register niet mogelijk is.