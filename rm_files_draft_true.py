import os
import yaml

def remove_draft_files(directories):
    for directory in directories:
        for root, _, files in os.walk(directory):
            for file in files:
                if file.endswith('.md'):
                    file_path = os.path.join(root, file)
                    with open(file_path, 'r') as stream:
                        try:
                            content = yaml.safe_load(stream)
                            if content.get('draft') == True:
                                print(f"Would remove draft file: {file_path}")
                        except yaml.YAMLError as exc:
                            print(f"Error parsing YAML file {file_path}: {exc}")

if __name__ == "__main__":
    directories = ["/content/concepten", "/conten/prototype"]  # Replace with the paths to your directories
    remove_draft_files(directories)
