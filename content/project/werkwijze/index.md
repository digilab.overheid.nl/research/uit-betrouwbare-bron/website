---
title: Werkwijze
draft: false
date: 2024-03-26T12:52:22.032Z
description: Hoe UBB tot resultaten komt
weight: 2
---


‘Uit betrouwbare bron’ omvat drie hoofdactiviteiten:
1. **Ontwerpen en ontwikkelen.** Het ontwerpen, toepassen en beproeven van kenmerken van een capabel register in het prototype.
2. **Documenteren.** Het zodanig beschrijven van concepten, capability’s en functionaliteit dat die in (andere) applicaties kunnen worden toegepast. Deze beschrijvingen zouden na afloop van het project ook kunnen dienen als basis voor handreikingen of standaarden. 
3. **Toetsen en uitdragen.** Het uitdragen van onze resultaten en het toetsen van bevindingen bij betrokkenen en belanghebbenden.

Deze activiteiten worden parallel aan elkaar uitgevoerd. Tegelijkertijd zorgen onderlinge afhankelijkheden ervoor dat we op verschillende momenten in verschillende mate aandacht aan bovengenoemde activiteiten zullen besteden.

Om te kunnen documenteren moeten we immers eerst begrijpen wat we van IT-systemen realistisch kunnen verwachten. Dat betekent dat we een eindje onderweg moeten zijn met ontwerpen en ontwikkelen. Toetsen en uitdragen is dan weer makkelijker als het materiaal wat onze bevindingen onderschrijft begrijpelijk en toegankelijk beschreven is. Wat betekent dat we een aardig deel van onze bevindingen hebben gedocumenteerd.

De verwachte aandachtsverdeling over de verschillende activiteiten voor de projectperiode is hieronder geïllustreerd.

<img src="../../img/projectactiviteiten.png" />

## Werkwijze

De doelen van het project ‘Uit betrouwbare bron’ zijn ambitieus, terwijl motivatie, aanleiding en behoefte reden zijn om de blik ver vooruit te richten. Aan dit vernieuwende karakter kleven drie belangrijke risico’s:

1. Wat we conceptueel willen, blijkt technisch niet haalbaar;
2. Wat we technisch kunnen, blijkt binnen een realistisch domein niet toepasbaar, en
3. Wat binnen een realistisch domein toepasbaar is, blijkt voor gebruikers niet werkbaar te zijn.

## Fasering

Optreden van deze risico’s kunnen we alleen uitsluiten door helemaal niet te innoveren. Dat is wat ons betreft geen optie. We kiezen in plaats daarvan voor een ‘fail as early as possible’-aanpak die de schade beperkt als één van deze risico’s bewaarheid wordt. Bij deze werkwijze doorlopen we een vijftal fases (Figuur 1):

<img src="../../img/ontwikkelaanpak.png" alt="figuur 1" />

1. **Vaststellen ontwerpfunctionaliteit.** We bepalen op hoofdlijnen welke capability’s en functionaliteit het register moet kunnen invullen.
2. **Toets op technische haalbaarheid.** De gewenste ontwerpfunctionaliteit wordt in een prototype gerealiseerd. Het ontwikkelproces helpt ons de concepten waarmee we werken beter te begrijpen. Deze fase eindigt wanneer duidelijk is of we de gewenste ontwerpfunctionaliteit daadwerkelijk kunnen implementeren. Zo nee, dan passen de we de ontwerpfunctionaliteit aan. Zo ja, dan gaan we door naar de volgende fase.
3. **Toets op realistische toepasbaarheid.** We toetsen aan de hand van modellen uit één of beide Proof of concept-domeinen ([WOZ](../../pocwoz) en Wegennetwerkregistratie) of het prototyperegister met concrete domeinhandelingen en -data overweg kan. Dit proces geeft ons inzicht in hoe het register zich gedraagt als daarin realistische datasets worden verwerkt. Deze fase eindigt wanneer duidelijk is dat het register realistische data volledig, betrouwbaar en performant kan verwerken. Lukt dit niet, dan passen de we ontwerpfunctionaliteit aan. Lukt dat wel, dan gaan we door naar de volgende fase.
4. **Toets op praktische werkbaarheid.** We toetsen met domeinprogrammeurs uit verschillende doelgroepen of het prototyperegister voor hen configureerbaar - dus in te richten voor gebruik in een specifiek domein – is. Dit vertelt ons of de functionaliteit van het register voldoende begrepen wordt om niet-ingewijden in staat te stellen daarmee praktisch aan de slag te gaan. Blijkt dit niet zo te zijn, dan passen we ontwerpfunctionaliteit aan. Blijkt dit wel zo te zijn, dan gaan we door naar de volgende fase.
5. **Documenteren en uitdragen.** Als de functionaliteit van het ontwikkelde register voldoet aan de eisen van technische haalbaarheid, realistische toepasbaarheid en praktische werkbaarheid, documenteren we de capability’s, functionaliteit en API-specificaties gedetailleerd. Eveneens documenteren we tijdens het ontwikkelproces gestelde vragen, genomen besluiten en ‘geleerde lessen’. Die kunnen immers waardevol zijn voor partijen die zelf een ‘capabel’ register willen (laten) ontwikkelen.