---
title: "Contact"
description: "Contact met 'Uit betrouwbare bron'"
layout: single
weight: 1
---

## Contact

Contact opnemen? Mail Ivo Hendriks ([ivo.hendriks@vng.nl](mailto:ivo.hendriks@vng.nl)).