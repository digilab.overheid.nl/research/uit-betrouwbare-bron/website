---
title: Uitgangspunten voor het prototype
type: document
category: prototype
navigation: Architectuur > Uitgangspunten
preliminary: true
draft: false
layout: single
weight: 1
---

Het registerprototype dat we als onderdeel van aan ‘Uit betrouwbare bron’ ontwikkelen, dient in de eerste plaats aan te tonen dat de in de vorige paragraaf beschreven capability’s functionaliteit technisch geïmplementeerd en praktisch gebruikt kunnen worden.

Voor het prototype hebben we daarnaast twee aanvullende doelen gedefinieerd:

## Complexiteit verbergen

Een capabel register kán veel meer functionaliteit leveren dan de registers die we nu kennen. Zo is het mogelijk op gedetailleerder niveau aan te wijzen welke handeling nu precies tot welke (historische) verandering in vastgelegde gegevens heeft geleid. Dit is vanuit het oogpunt van verantwoording wenselijk, maar brengt ook extra complexiteit met zich mee. Partijen die met het register te maken krijgen, zoals domeinexperts die het register moeten inrichten, softwareleveranciers die daarop moeten aansluiten en gebruikers die het bijhouden of bevragen, kunnen met deze complexiteit geconfronteerd worden.

Als deze complexiteit hen iets oplevert – zoals een beter begrip van wat een gegeven betekent, is dit geen probleem. We willen echter voorkomen dat complexiteit partijen hindert in gevallen waarin die geen meerwaarde heeft – denk aan een gebruiker die eenvoudigweg een overzicht wil zien van de ‘nu’ geldige gegevens, zonder zich te bekommeren om de historie of registratiecontext daarbij. Of aan de domeinexpert die het register wil inrichten zonder geconfronteerd te worden met ‘nieuwe’ vastleggingsconcepten en ingewikkelde programmacode.

Het uitgangspunt voor het prototype is daarom dat softwarematige complexiteit zoveel mogelijk onzichtbaar blijft en alleen tevoorschijn komt als dat nodig en zinvol is. Hieraan geven we invulling door bij ontwikkeling van het prototype steeds – zowel waar het gaat om softwarearchitectuur als ordening van functionaliteit – te denken vanuit ‘lagen’ die onderliggende complexiteit voor de gebruiker zoveel mogelijk verbergen.

## Atomaire claims

Het detailniveau waarop we bepaalde capability’s willen realiseren is bij gebruik van relationele databases – het op dit moment dominante vastleggingsparadigma – niet of nauwelijks haalbaar. Zo kunnen we in onze UML-modellen wel aangeven van ieder attribuut historie bij te willen houden, maar als het aankomt op technische implementatie blijkt realisatie in het beste geval complex, duur en ten koste te gaan van performance en onderhoudbaarheid. En in het slechtste geval blijkt het eenvoudigweg onmogelijk. In het prototyperegister willen we daarom een alternatieve vastleggingspraktijk beproeven, gebaseerd op atomaire claims als kleinste vastleggingseenheid.