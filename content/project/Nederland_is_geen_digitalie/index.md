---
title: Nederland is geen Digitalië
draft: false
date: 2025-02-2025
description: Nederland is geen Digitalië - werkende ketens voor iedereen (blogpost in het kader van 'Dag van de toekomstige ketens')
weight: 1
layout: single
preliminary: false
category: project
doc-type: blog
---

<img src="../../img/4505-digitalie.png" alt="Impressie van Digitalië (credit: MvA)">

In het atollenrijk Digitalië zijn alle eilanden door indrukwekkende hogesnelheidsinfrastructuur verbonden. Maar die is alleen toegankelijk voor een onbelemmerd op de automatische piloot rondflitsende _happy flew_[^1].

Alle pogingen om ook de _unhappy flaw_[^2] toe te laten liepen spaak: te moeilijk, te duur. Want deze gevallen zonder vaste bestemming negeerden zorgvuldig vastgestelde gewichts- en hoogtebeperkingen, bewogen zich tegen de rijrichting in en eisten afritten op onmogelijke plaatsen.

En daarom bevaart de _unhappy flaw_ tot op heden de mistige zeeën van Digitalië in een haveloos roeibootje.

## Werkende ketens voor iedereen

De Nederlandse digitale overheid kan geen Digitalië zijn.

Ze moet werken voor de overweldigende meerderheid van de gevallen waarin alles gesmeerd verloopt. En óók voor de kleine minderheid van gevallen waarin iets misgaat.

Dat laatste is uitdagend, zo is de afgelopen jaren meermaals gebleken. Zeker als meerdere partijen samenwerken.

Dan ontstaat spraakverwarring. Wat glashelder is voor de één, kan voor een ander immers koeterwaals lijken.

Worden fouten uitvergroot. Want hoe repareer je een gebrek in andermans voorziening?

Blijken we onvolledig geïnformeerd. Omdat we gewijzigde gegevens zonder kennis van de aanleiding voor die verandering niet altijd goed begrijpen.

En ontstaat onzekerheid. Wie durft nog de consequenties van een handeling te garanderen als het einde van een keten van mogelijke gevolgen in nevelen is gehuld?

Tijdens de _Dag van de toekomstige ketens_ willen we samen met jullie, aan de hand van casuïstiek en met speciale aandacht voor de rol van capabele registers, verkennen wat er nodig is voor digitale ketens die niet alleen de _happy flew_ bedienen.

Binnenkort volgt meer informatie over aanmelding voor, en inhoud van deze dag.

[^1]: _Happy flew_ is een opzettelijke verbastering van _happy flow_; de ongehinderd fladderende 'happy few' onder geautomatiseerde handelingen die binnen informatiesystemen zonder vertraging of andere problemen kunnen worden verwerkt.
[^2]: _Unhappy flaw_ is een opzettelijke verbastering van _unhappy flow_; de (hopelijk) uitzonderlijke handelingen die binnen informatiesystemen niet, of gebrekkig geautomatiseerd verwerkt kunnen worden.

_**Auteur**: Ivo Hendriks werkt voor de Vereniging van Nederlandse Gemeenten aan het project ['Uit Betrouwbare Bron'](https://uitbetrouwbarebron.nl/)_