---
title: "Dag van de toekomstige registraties"
draft: false
weight: 3
---

Op de dag van de toekomstige registraties is door Uit betrouwbare bron een presentatie verzorgd. 

Op deze dag, georganiseerd door team Digilab, is verkend hoe we voor de toekomst registers kunnen bouwen die voldoen aan publieke waarden, die wendbaar zijn en een traceerbare vertaling van wet naar uitvoering borgen. 
 
Sprekers op deze dag waren: 

- Arjan Widlak, hij nam ons mee in de historie van de bureaucratie en waar het in de digitalisering soms misgaat. Hij illustreerde hoe burgers klem komen te zitten door gegevensdeling.
- Mariette Lokin, als pleitbezorger van een Weberiaanse bureaucratie in de goede zin des woords, gaf zij inzicht in de methode Wendbare Wetsanalyse en de voordelen van juridisch-technische samenwerking, geïnspireerd door DevOps: “LegOps”.
- Wicher Minnaard deelde zijn ervaringen in Timor-Leste, waar hij met een klein team een nieuwe overheidsregistratie opzette. Hij benadrukte het belang van het kunnen terugkijken en rekenen in registraties.
- Jeanot Bijpost besprak hoe we de werkelijkheid in toekomstige registraties het beste kunnen benaderen, met aandacht voor het opdelen van claims en het meenemen van context bij registratiehandelingen.

[De handouts van de bijdrage door Jeanot Bijpost zijn hier te bekijken](./20240625-Dag_van_de_toekomstige_registraties-UBB_Handouts.pdf)

