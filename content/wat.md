---
title: Wat is 'Uit betrouwbare bron'?
description: Wat is 'Uit betrouwbare bron'?
draft: false
layout: single
weight: 1
---

**We willen data vaker federatief en ‘bij de bron’ gebruiken. Dit betekent dat de informatiebehoeften van afnemers 'bij de bron' moeten worden ingevuld. Het project ‘Uit betrouwbare bron’ onderzoekt en beproeft wat we op moment van registratie moeten kunnen en doen om afnemers goed te bedienen.**

Deze ‘bijhoudingsvereisten’ beschrijven we in de vorm van capabilities en functionaliteit. Hoe deze eruit zien, ontdekken we door het bouwen van een prototype van een ‘capabel’ register.

De bruikbaarheid van het prototype tonen we aan door voor twee domeinen (deel)registers in te richten. Deze registers gelden als ‘proof of concept’; ze zijn dus niet geschikt voor toepassing in productie-omgevingen. 

Aan het eind van het project zijn we dankzij ervaring en documentatie bekwame opdrachtgevers voor ontwikkeling van capabele registers die passen binnen een federatief datastelsel.

Het project ‘Uit betrouwbare bron’ wordt uitgevoerd door [VNG Realisatie](https://vng.nl/artikelen/vng-realisatie) en betaald uit het [innovatiebudget GDI](https://open.overheid.nl/documenten/0ce17d4b-5d7c-433b-b5b6-1ead1e99b79a/file).

## Projectdoel

Het idee van een federatief datastelsel is gebaseerd op een landschap waarin een groot aantal registers ‘bij de bron’ bevraagd kan worden.

Om van dit idee een succes te maken, moeten de gegevens in deze registers van aantoonbaar goede kwaliteit zijn. En om te voorkomen dat afnemers met het oog op verantwoording zelf kopiegegevens gaan bijhouden, moet ‘vanuit de bron’ aan zoveel mogelijk gebruiksscenario’s invulling geven. Voorbeelden daarvan zijn het leveren van historische gegevens of informatie over de gebeurtenis die aanleiding gaf voor registratie van gegevens.  Hieruit ontstaan eisen aan de manier waarop we gegevens registreren.

‘Capabele’ registers geven invulling aan deze eisen. Het formuleren van deze eisen en toetsen of deze eisen implementeerbaar zijn is  het aandachtsgebied van het project ‘Uit betrouwbare bron’.

Alle activiteiten die we binnen dit project uitvoeren, dragen bij aan één overkoepelend doel: dat we als overheid straks in staat zijn om op te treden als deskundig en bekwaam opdrachtgever bij ontwikkeling of verwerving van de ‘capabele’ registers die we nodig hebben als fundament van onze informatievoorziening. Om dat doel te bereiken, gaan we een aantal zaken opleveren. Die worden hieronder beschreven.

## ‘Capabiliteit’

We gaan beschrijven hoe registers invulling kunnen geven aan maatschappelijke waarden rondom betrouwbaarheid en verantwoording, daaruit voortkomende wettelijke verplichtingen en de behoeften van data-afnemers op dit gebied. Het resultaat vormt het conceptuele, domein- en vastleggingspraktijkonafhankelijke skelet van het ‘capabele’ register, beschreven in twee delen:

1. een overzicht van de capability’s die een register op het gebied van bijhouding kan of moet invullen, met per capability een beschrijving van de systeemfunctionaliteit die hiervoor nodig is. 
2. een handreiking voor het aanbieden aan andere systemen van bovengenoemde systeemfunctionaliteit door middel van API-interfaces.

## Prototype

De toepasbaarheid van de concepten die we beschrijven gaan we beproeven door implementatie in een prototype van een capabel register. Aanvullend toetsen we met het prototype twee veronderstellingen:

1. Dat we het aanbieden van functionaliteit die de gevonden capability’s invult makkelijker kunnen maken voor programmeurs in toepassingsdomeinen. Dit onder meer door het hanteren van een gelaagde architectuur waarin complexe programmacode die nodig is voor het invullen van de capability’s wordt verborgen achter een eenvoudiger te programmeren framework.
2. Dat een alternatieve vastleggingspraktijk bijdraagt aan een meer volledige invulling van de gewenste capability’s. Dit door een prototype in te richten voor vastlegging van atomaire claims.

Het prototype is nadrukkelijk een ‘proof of concept’ van een ‘capabel’ register. Het voldoen aan niet-functionele eisen rondom bijvoorbeeld performance en betrouwbaarheid zien we als cruciaal ‘bewijs’ voor de toepasbaarheid, en nemen we dus mee in het prototype. Maar ‘Uit betrouwbare bron’ levert geen productiesoftware, en dus ook geen in de praktijk te gebruiken register op.

## Proof of concepts

Het registerprototype valideren we door het als ‘proof of concept’ te richten voor twee concrete toepassingsdomeinen:

1.	Het WOZ-domein
2.	Registratie van wegennetwerkgegevens