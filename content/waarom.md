---
title: Waarom 'Uit betrouwbare bron'?
description: Waarom 'Uit betrouwbare bron'?
draft: false
layout: single
weight: 1
---

**Het project ‘Uit betrouwbare bron’ helpt geconstateerde gebreken in de informatievoorziening van de overheid op te lossen en ondersteunt streefbeelden bij de richting waarin die zich zou moeten ontwikkelen.**

## Capabele(r) IT

De [Werkagenda Waardengedreven Digitaliseren](https://www.digitaleoverheid.nl/kabinetsbeleid-digitalisering/werkagenda/) noemt “een veilige, wettelijke en waardengedreven opslag en omgang met data, waardoor vertrouwen onder burgers ontstaat” als doel. We zien echter dat bestaande IT-systemen overheden niet (eenvoudig) in staat stellen invulling te geven aan waarden als transparantie en accountability. Dit terwijl vanuit de maatschappij, en in het verlengde daarvan de wetgever, de vraag hiernaar steeds luider klinkt.

**Dit betekent dat er grote behoefte bestaat aan ‘capabele’ IT-systemen die het handelen van de overheid inzichtelijk en navolgbaar (kunnen) maken. Deze behoefte heeft met name betrekking op systemen waarin overheidsgegevens worden geregistreerd – ofwel de ‘registers’.**

## Het begint bij vastlegging

Het [Programmeringsplan GDI (2023)](https://www.rijksoverheid.nl/documenten/rapporten/2022/12/02/gdi-programmeringsplan-2023) stelt dat gebruikers behoefte hebben aan een organisatie-overstijgend gegevenslandschap, waarbinnen onder andere een betere gegevenskwaliteit gegarandeerd kan worden. Dit organisatie-overstijgend gegevenslandschap krijgt onder de naam [‘Federatief Datastelsel’](https://www.noraonline.nl/wiki/FDS_Basis_concept) langzaam vorm.

We zien echter dat op realisatie van dit landschap gerichte inspanningen tot nu toe vooral zijn gericht op het beschikbaar maken van gegevens. Voor vastlegging van gegevens is daarentegen beperkt aandacht. Terwijl juist bij vastlegging – ook wel ‘bijhouding’ genoemd – de basis voor succesvolle beschikbaarstelling wordt gelegd. Immers, gegevens die in de eerste instantie niet (goed) zijn geregistreerd, kunnen ook niet beschikbaar komen voor gebruik.

**Om verantwoord (federatief) gebruik van gegevens mogelijk te maken, moeten we dus begrijpen wat ‘capabele’ registers op het moment van bijhouding moeten kunnen. Of met andere woorden: welke capability’s registers op dat gebied moeten invullen.**

## Werken met API’s

De [API-strategie voor de NL-overheid](https://docs.geostandaarden.nl/api/API-Strategie) stelt dat “iedere organisatie voor ontsluiting van data en functionaliteit altijd ‘API first’ [zou] moeten werken”.

**Dit betekent dat we moeten beschrijven hoe de systeemfunctionaliteit die bovengenoemde capability’s invult middels API’s aan andere systemen aangeboden kan worden.**

## Eenvoudiger mechanismen

‘Data bij de bron’ is een kernprincipe van het [Federatief Datastelsel](https://www.noraonline.nl/wiki/FDS_Basis_concept). We stellen dat gebruiken van ‘data bij de bron’ een systeem van registers vereist. Zo’n systeem omvat niet alleen de nu al relatief ‘capabele’ basisregistraties, maar juist ook op dit moment nog niet als waardevol herkende registers waarin gegevens zijn opgeslagen die – zij het wellicht voor een beperktere groep afnemers – grote gebruikswaarde hebben.

Om te voorkomen dat deze afnemers gegevens onjuist interpreteren of voor eigen verantwoording kopieën moeten aanleggen, is het nodig dat ook deze registers bijvoorbeeld gebeurtenisinformatie en historische gegevens kunnen leveren – en dus ‘capabel’ worden. De bijhorende ontwerp- en implementatiecomplexiteit verhindert op dit moment echter vaak dat de ‘business case’ hiervoor rondkomt.

**Grootschalig ‘bevragen bij de bron’ vereist dus mechanismen die het bijhouden van gegevens die afnemers nodig hebben voor correcte interpretatie en verantwoording vereenvoudigen.**

## Anders digitaliseren

[NL DIGIbeter](https://open.overheid.nl/repository/ronl-71c5e1c9-04ba-49d7-9058-b480a1447be0/1/pdf/NL%20DIGIbeter%202020%20Agenda%20Digitale%20Overheid.pdf) noemt digitaliseren “meer dan het digitaliseren van papieren processen”. Toch is dit wat we de afgelopen decennia vooral hebben gedaan. We verwachten dat onze bestaande vastleggingspraktijk, gebaseerd op vertaling van papieren processen naar relationele databases, niet goed - of tenminste niet het best - aansluit bij wat we van ‘capabele’ registers verwachten.

**We moeten daarom uitproberen hoe een ‘ideale’ vastleggingspraktijk eruit ziet, en welke beperkingen de bestaande vastleggingspraktijk ten opzichte daarvan opwerpt.**

## Beter opdrachtgeverschap

Het working paper [‘Deskundigheid als koopwaar’](https://www.wrr.nl/binaries/wrr/documenten/working-papers/2023/12/07/deskundigheid-als-koopwaar/WP056+Deskundigheid+Als+Koopwaar.pdf) van de Wetenschappelijke Raad voor het Regeringsbeleid adviseert overheden strategischer na te denken over welke deskundigheden intern georganiseerd worden en welke van buitenaf betrokken kunnen worden. Als het gaat om ontwerp en ontwikkeling van ‘capabele’ registers constateren we zowel binnen als buiten de overheid een gebrek aan kennis en kunde. We weten niet wat we de markt moeten vragen, terwijl de markt weet niet wat ze ons moet leveren.

**Willen we (later) als deskundige partner opdracht geven aan marktpartijen voor ontwikkeling van ‘capabele’ registers, dan moeten we met de ontwikkeling daarvan dus eerst zelf, binnen de overheid, ervaring opdoen.**

## ‘The proof of the pudding…’

Het resultaat van al het bovenstaande heeft alleen bestaansrecht als we aantonen dat wat we beschrijven en coderen in de praktijk ook toepasbaar is.
