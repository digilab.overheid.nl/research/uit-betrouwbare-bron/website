---
title: Proof of Concept WOZ
draft: false
date: 2024-03-26T12:52:22.032Z
description: Toelichting op de Proof of concept voor de WOZ
layout: single
weight: 2
---

![](../img/woz-image.png)

## Inleiding

Om de inzichten die we in het project “Uit betrouwbare bron” opdoen te toetsen voeren we parallel aan het opdoen van die inzichten een Proof of Concept uit die gebaseerd is op een praktijksituatie. Deze PoC wordt gedaan op het WOZ-domein.

## Scope

Voor de Proof of Concept gaan we een deel van het registratieproces van een WOZ-object beproeven. De taxatie en waardevaststelling-processen worden buiten beschouwing gelaten in de PoC. Het Informatiemodel dat nu is opgesteld is dan ook beperkt tot die objecten die relevant zijn voor het registratieproces. Daarnaast is voor het leggen van relaties naar persoons- en organisatie-registraties de structuur die in het kader van klantinteracties is ontwikkeld overgenomen. Deze wijze van registreren van partijen wordt onderdeel van de Proof of Concept.

In het informatiemodel is de scope beschreven van de gegevens die we in deze PoC gaan vastleggen. Deze scope zal gedurende de proof of concept nog kunnen wijzigen. We documenteren eventuele wijzgingen op de scope.

## Casussen

De casuïstiek die we gaan beproeven heeft hoofdzakelijk te maken met de vastlegging van gegevens. We beschrijven een aantal casussen waarmee we zoveel mogelijk registerfunctionaliteit willen beproeven.

Details over de Proof of Concept zijn te vinden op de <a href="https://poc-digilab-overheid-nl-research-uit-betrouwbare-dda02f873817b1.gitlab.io" target=_blank>website van de PoC</a>
