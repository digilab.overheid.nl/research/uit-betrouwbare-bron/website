---
title: Concepten
draft: false
date: 2023-11-22T08:35:34+02:00
description: Documentatie over de concepten.
---

Deze pagina's beschrijven de conceptuele, implementatie-onafhankelijke resultaten van 'Uit betrouwbare bron'. Die bestaan onder meer uit thema's, capability's en functies. 


