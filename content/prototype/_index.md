---
title: Prototype
draft: false
date: 2023-11-22T08:35:34+02:00
description: Documentatie over de het prototype.
---

Deze pagina's beschrijven de resultaten van 'Uit betrouwbare bron', voortkomend uit de ontwikkeling van een prototype-register. Daaronder architectuur, vragen en besluiten.